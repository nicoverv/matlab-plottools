function test_suite = test_plottools
initTestSuite;

function test_add_one_plot
global plotlist;
plotlist = cell(0);
assert(isempty(plotlist));
addplot(@plot, 1:3, [1 1 1], 'r');
assert(length(plotlist) == 1);
assert(isa(plotlist{1}{1}, 'function_handle'))
assert(iscell(plotlist{1}{2}))

function test_add_two_plots_on_same_subplot
global plotlist;
plotlist = cell(0);
assert(isempty(plotlist));
addplot(@plot, 1:3, [1 1 1], 'r', @surf, rand(2,2));
assert(length(plotlist) == 1);
% test first
assert(isa(plotlist{1}{1}, 'function_handle'))
assert(iscell(plotlist{1}{2}))
assert(length(plotlist{1}{2}) == 3)
f = functions(plotlist{1}{1});
assert(strcmp(f.function, 'plot') == 1)
% test second
assert(isa(plotlist{1}{3}, 'function_handle'))
assert(iscell(plotlist{1}{4}))
f = functions(plotlist{1}{3});
assert(length(plotlist{1}{4}) == 1)
assert(strcmp(f.function, 'surf') == 1)

function test_add_two_plots_on_different_subplot
global plotlist;
plotlist = cell(0);
assert(isempty(plotlist));
addplot(@plot, 1:3, [1 1 1], 'r');
addplot(@surf, rand(2,2));
assert(length(plotlist) == 2);
% test first
assert(isa(plotlist{1}{1}, 'function_handle'))
assert(iscell(plotlist{1}{2}))
f = functions(plotlist{1}{1});
assert(strcmp(f.function, 'plot') == 1)
% test second
assert(isa(plotlist{2}{1}, 'function_handle'))
assert(iscell(plotlist{2}{2}))
f = functions(plotlist{2}{1});
assert(strcmp(f.function, 'surf') == 1)

function test_reset
global plotlist;
addplot(@plot, 1:3, [1 1 1], 'r');
addplot(@surf, rand(2,2));
assert(~isempty(plotlist));
resetplots;
assert(isempty(plotlist));

function test_add2plots_single
global plotlist;
resetplots;
addplot(@plot, 1:3, [1 1 1], 'r');
addplot(@surf, rand(2,2));
assert(length(plotlist) == 2);
add2plots(1, @title, 'test');
assert(length(plotlist) == 2);
assert(length(plotlist{1}) == 2*2);
assert(isa(plotlist{1}{3}, 'function_handle'))
assert(iscell(plotlist{1}{4}))
f = functions(plotlist{1}{3});
assert(strcmp(f.function, 'title') == 1)
assert(length(plotlist{1}{4}) == 1)

function test_add2plots_multiple
global plotlist;
resetplots;
addplot(@plot, 1:3, [1 1 1], 'r');
addplot(@surf, rand(2,2));
assert(length(plotlist) == 2);
add2plots([1 2], @title, 'test');
assert(length(plotlist) == 2);
for n = 1:2
    assert(length(plotlist{n}) == 2*2);
    assert(isa(plotlist{n}{3}, 'function_handle'))
    assert(iscell(plotlist{n}{4}))
    f = functions(plotlist{n}{3});
    assert(strcmp(f.function, 'title') == 1)
    assert(length(plotlist{n}{4}) == 1)
end 

function test_add2plots_all
global plotlist;
resetplots;
addplot(@plot, 1:3, [1 1 1], 'r');
addplot(@surf, rand(2,2));
assert(length(plotlist) == 2);
add2plots(@title, 'test');
assert(length(plotlist) == 2);
for n = 1:2
    assert(length(plotlist{n}) == 2*2);
    assert(isa(plotlist{n}{3}, 'function_handle'))
    assert(iscell(plotlist{n}{4}))
    f = functions(plotlist{n}{3});
    assert(strcmp(f.function, 'title') == 1)
    assert(length(plotlist{n}{4}) == 1)
end 

function test_holdplot
global plotlist;
resetplots;
addplot(@plot, 1:3, [1 1 1], 'r');
addplot(@surf, rand(2,2));
assert(length(plotlist) == 2);
holdplot(@title, 'test');
assert(length(plotlist) == 2);
assert(length(plotlist{1}) == 2);
assert(length(plotlist{2}) == 2*2);
assert(isa(plotlist{2}{3}, 'function_handle'))
assert(iscell(plotlist{2}{4}))
f = functions(plotlist{2}{3});
assert(strcmp(f.function, 'title'))
assert(length(plotlist{2}{4}) == 1)

function test_holdplot_empty
global plotlist;
resetplots;
assert(isempty(plotlist));
holdplot(@title, 'test');
assert(length(plotlist) == 1);
assert(length(plotlist{1}) == 2);
assert(isa(plotlist{1}{1}, 'function_handle'))
assert(iscell(plotlist{1}{2}))
f = functions(plotlist{1}{1});
assert(strcmp(f.function, 'title'))
assert(length(plotlist{1}{2}) == 1)
