function plotlist = addplot(varargin)
%ADDPLOT add a subplot
%   ADDPLOT(FUNC, OPTIONS) adds the FUNC plot command as a new subplot in the
%   plotlist and passes the attached options. Options are normal arguments of
%   the plot command, e.g.:
%   
%       plot(x, y, 'r.'); % becomes
%       addplot(@plot, x, y, 'r.');
%
%   ADDPLOT(FUNC1, OPTIONS1, FUNC2, OPTIONS2, ...) adds all plot commands,
%   e.g.:
%    
%       addplot(@plot, x, y, 'r', @surf, a, b, c, @title, 'nice');
%
%   If an option equals 'gca' or 'gcf' its evaluation is delayed until all
%   plots are shown using showplots. Hence, in
%
%       addplot(@plot, x, y, @set, gca, 'ColorOrderIndex', 1);
%
%   gca is evaluated after the first plot command. If this is not the desired
%   behavior, gca or gcf should be stored in a variable, e.g., 
%
%       ax = gca;
%       addplot(@plot, x, y, @set, ax, 'ColorOrderIndex', 1);
    
% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2016/03/31   NV      Added gcf/gca parsing
% - 2015/02/03   NV      Initial version
    
    global plotlist;
    i = find(cellfun(@(f) isa(f, 'function_handle'), varargin));
    i = [i, length(varargin)+1];
    t = cell(0);
    for k = 1:length(i)-1
        t{end+1} = varargin{i(k)};
        t{end+1} = varargin(i(k)+1:i(k+1)-1);
        for l = 1:length(t{end})
            if strcmpi(inputname(i(k)+l), 'gca')
                t{end}{l} = '@GCA';
            elseif strcmpi(inputname(i(k)+l), 'gcf')
                t{end}{l} = '@GCF';
            end 
        end 
    end 
    plotlist{end+1} = t;
end
