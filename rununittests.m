addpath('../matlabutils/xunit/')
import xunit.*;
import utils.*;
addpath(genpath('../matlabutils/coverage'));
import edu.stanford.covert.test.Coverage;

name = 'Plot tools test';
description = 'Unit test for plot tools.';
outFileName = 'tests.plottools.xml';
tests = 'tests';
monitor = XMLTestRunDisplay(name, description, outFileName);
profile clear
profile on
runtestsxml(monitor, tests);
profile off
report = Coverage('.');
report.exportXML('coverage.xml');

%exit;
