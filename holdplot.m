function holdplot(varargin)
%HOLDPLOT adds commands to the last defined plot
%   HOLDPLOT(FUNC1, OPTIONS1) adds FUNC and its OPTIONS to the last defined
%   plot. If the plot list is empty, a new subplot is created.
%   
    
% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2015/02/03   NV      Initial version
    
    global plotlist;
    if isempty(plotlist) 
        addplot(varargin{:});
        return 
    else 
        add2plots(length(plotlist), varargin{:});
    end
end
