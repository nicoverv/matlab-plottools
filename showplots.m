function showplots(rows,cols)
%SHOWPLOTS shows the defined plots
%   SHOWPLOTS(ROWS) defines ROWS rows of subplots and display the defined
%   plots. 
%
%   SHOWPLOTS(ROWS, COLS) defines ROWS rows and COLS columsn of subplots and
%   display the defined plots. If ROWS = [], it is determined automatically. 
%   
%   SHOWPLOTS() is equivalent to SHOWPLOTS(1);
    
% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2016/03/31   NV      Added cols & gcf/gca parsing
% - 2015/02/03   NV      Initial version
    
    if nargin == 0, rows = 1; end
    
    global plotlist;
    N = length(plotlist);
    if N > 1
        if nargin < 2 || isempty(cols), cols = ceil(N/rows); end
        if nargin >= 2 && isempty(rows), rows = ceil(N/cols); end
        for k = 1:length(plotlist)
            subplot(rows,cols,k)
            doplot(plotlist{k});
        end 
    elseif N == 1
        doplot(plotlist{1});
    end
    
    function doplot(cmd)
        for l = 1:2:length(cmd)
            for m = 1:length(cmd{l+1})
                if ischar(cmd{l+1}{m}) 
                    if strcmpi(cmd{l+1}{m}, '@GCA')
                        cmd{l+1}{m} = gca;
                    elseif strcmpi(cmd{l+1}{m}, '@GCF')
                        cmd{l+1}{m} = gcf;
                    end 
                end 
            end 
            cmd{l}(cmd{l+1}{:});
            if l == 1, hold on; end
        end
    end 
end
