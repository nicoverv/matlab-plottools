function resetplots()
%RESETPLOTS removes all subplots from the list
    
% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2015/02/03   NV      Initial version
    
    global plotlist;
    plotlist = cell(0);
end
