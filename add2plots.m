function add2plots(plots, varargin)
%ADDPLOT add commands to given plots
%   ADDPLOT(PLOTS, FUNC1, OPTIONS1, ...) adds the given functions FUNC1, ...
%   and OPTIONS1 to all plots defined by PLOTS. 
%   
%   ADDPLOT(FUNC1, OPTIONS1, ...) adds the given functions and options to all
%   available plots.
%
%   If an option equals 'gca' or 'gcf' its evaluation is delayed until all
%   plots are shown using showplots. Hence, in
%
%       addplot(@plot, x, y, @set, gca, 'ColorOrderIndex', 1);
%
%   gca is evaluated after the first plot command. If this is not the desired
%   behavior, gca or gcf should be stored in a variable, e.g., 
%
%       ax = gca;
%       addplot(@plot, x, y, @set, ax, 'ColorOrderIndex', 1);
    
% Author(s): Nico Vervliet       (Nico.Vervliet@esat.kuleuven.be)
%
% Version History:
% - 2016/03/31   NV      Added gcf/gca parsing
% - 2015/02/03   NV      Initial version
    
    global plotlist;
    
    if ~isnumeric(plots)
        varargin = [{plots}, varargin];
        plots = 1:length(plotlist);
    end
        
    i = find(cellfun(@(f) isa(f, 'function_handle'), varargin));
    i = [i, length(varargin)+1];
    t = cell(0);
    for k = 1:length(i)-1
        t{end+1} = varargin{i(k)};
        t{end+1} = varargin(i(k)+1:i(k+1)-1);
        for l = 1:length(t{end})
            if strcmpi(inputname(i(k)+l+1), 'gca')
                t{end}{l} = '@GCA';
            elseif strcmpi(inputname(i(k)+l+1), 'gcf')
                t{end}{l} = '@GCF';
            end 
        end 
    end 
    for k = 1:length(plots)
        plotlist{plots(k)} = [plotlist{plots(k)} t];
    end 
end
