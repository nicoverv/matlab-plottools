Plottools
=========

A series of tools to simplify making a lot of plots in Matlab. Currently, the
focus is on making subplots easier. We will demonstrate the possibilities by
example.

We will work on the following example functions:
```matlab
t = linspace(0, pi, 20);
y1 = sin(t);
y2 = cos(t) + 1;
[X, Y] = meshgrid(1:10, 1:10);
Z = X.^2 + cos(Y);
```
Suppose we want to create four subplots in a two-by-two grid. The standard
matlab syntax would be:
```matlab
subplot(2,2,1)
plot(t, y1);
subplot(2,2,2)
plot(t, y1);
hold on
plot(t, y1 + y2, 'r')
subplot(2,2,3)
surf(X, Y, Z);
subplot(2,2,4)
plot(X, Y, Z + 3);
```
Using plot tools, we can do the following:
```matlab
resetplots; % clear the list
addplot(@plot, t, y1);
addplot(@plot, t, y2, @plot, t, y1 + y2, 'r');
addplot(@surf, X, Y, Z);
addplot(@surf, X, Y, Z + 3);
showplots(2); % plot in two rows
```
This is not only shorter, it is also more flexible. For the examples below, use
`showplots` to update the figure. All functions can be called inside functions.

* When we want to display all the plots in one row, we use:

        showplots(1);
  
* Add a title to the last plot:

        holdplot(@title, 'Surf plot');
        add2plots(4, @title, 'Surf plot'); % does the same
  
* Change the y-limits for both (co)sine plots:

        add2plots(1:2, @ylim, [-1 2]);

* The `gca` and `gcf` commands are evaluated when the subplots are plotted,
  e.g.,

        resetplots;
        addplot(@plot, rand(1, 5), @set, gca, 'ColorOrderIndex', 1, @plot, 1:5);
        showplots(1)
